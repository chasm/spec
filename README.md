Chasm
=====

> The C-like, holistic Assembler

(Programmers are bad at names, so perhaps Chasm is just a placeholder.)

Chasm wants to become an Universal Assmbler. That which C is often claimed to
be. Chasm has a C like syntax to lure unsuspecting programmers in just like Go
and Rust.

Goals
-----

* types, everywhere, unerasable, unquestionable
* stable memory model
* actual modules
* sensible error model
* integrate (safely) with existing C (compatible) libraries
